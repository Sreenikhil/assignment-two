// I am gonna do a crazy job
// Build an express app which has a timestamp middleware and Logging middleware

import {NextFunction, Request, Response, Application} from 'express'
import mongoose = require('mongoose');
const Retailer = require('./retailerSchema').Retailer;
import Express = require('express');
import {customMiddleware} from './Middlewares/cookie_middleware'
import {strongMiddleware} from './Middlewares/cookie_middleware'
import {abc} from './Middlewares/cookie_middleware'
const Sessions = require('./SessionSchema').Sessions;
import cookieParser = require('cookie-parser');
import nanoid = require('nanoid');
import bcrypt = require('bcrypt');
const dotenv = require('dotenv').config();


const app: Application = Express();
app.use(cookieParser(process.env.secrete));
app.use(Express.json());




const db_connect =  mongoose.connect("mongodb://localhost:27017/testdb", {useNewUrlParser: true} );
//
// -----------------------------   Inserting Data ------------------------------------------
// To insert all retailers in to the database
// Make sure that all post middleware passes through strong middleware
app.post("/retailer-reg",   [strongMiddleware({Company_name: 'string', Country_code: 'number', Phone_Number: 'number', Address: 'string', Country: 'string', City: 'string', Retailer_ID: 'string', password: 'string'})], async (request: Request, response: Response) => {
    console.log("I am in retailer registration");
    // Let me check for unique company name and retailer id
    const existsfalg = await Retailer.findOne( {$or: [
        {Company_name: {$eq: response.locals.strongParams['Company_name']}},
            {Retailer_ID: {$eq: response.locals.strongParams['Retailer_ID']}}
            ]});

    console.log("Existing retailer is: "+ existsfalg);
    if(existsfalg){
        return response.send("Duplicate retailer").status(200)
    }
    else {
        try {
            await bcrypt.hash(response.locals.strongParams['password'], 12, async (err, hash) => {
                if (err) {
                    console.log("Problem in doing hash" + hash);
                    return response.sendStatus(404)
                } else {
                    await Retailer.create({
                        Company_name: response.locals.strongParams['Company_name'],
                        Country_code: response.locals.strongParams['Country_code'],
                        Phone_Number: response.locals.strongParams['Phone_Number'],
                        Address: response.locals.strongParams['Address'],
                        Country: response.locals.strongParams['Country'],
                        City: response.locals.strongParams['City'],
                        Retailer_ID: response.locals.strongParams['Retailer_ID'],
                        password: hash
                    });
                    return response.sendStatus(200)
                }
            });
        } catch (err) {
            console.log("Error performing the request" + err.message)
        }
    }
 });

// customMiddleware({requireCookie: false})
// --------------------------   Handling session - Login Route  ------------------------------------
// Our job is to check if we can allow the user
// If Secure is made false I got a cookie
// Write code so that, if more than 5 sessions exists then remove one of them
app.post("/session", [
    customMiddleware({requireCookie: false}),
    strongMiddleware({Retailer_ID: 'string', password: 'string'})
   ] , async (request: Request, response: Response) =>{
    console.log("The cookie end point is hit");
    // If user cookie exixts do not ask for details and retrive corresponding user
    if(response.locals.userCookie) {
        console.log("I found a cookie in login route");
        console.log("I am in the main middleware");
        console.log(response.locals.userCookie);
        const retailer = await Sessions.findOne({Session_ID: {$eq: response.locals.userCookie}});
        if (retailer) {
            console.log("Retailer Exists");
            console.log("Concerned retailer is: " + retailer.Company_name);
            return response.send("You did a great login job").status(200)
        }
    }
        console.log("I am checking for username and password");
        // Logic to check if login is successful
        const retailer = await Retailer.findOne({Retailer_ID: {$eq: response.locals.strongParams['Retailer_ID']}});
        if (!retailer) {
            console.log("Invalid username/password");
            return response.sendStatus(400)
        }
        await bcrypt.compare(response.locals.strongParams['password'], retailer.password, async (err, result) => {
            if (err) console.log("There is an error checking password" + err.message);
            if (result) {
                console.log("I have successfully authenticated, now I am creating a session for the user");
                // store session id in the database
                let sessionid: string = nanoid();
                // I am omitting collisions because they occur 1 in 1 million
                console.log(sessionid);
                console.log("I have created a cookie for you ...");
                const num_of_sessions = await Sessions.find({Retailer_ID: {$eq: response.locals.strongParams['Retailer_ID']}});
                console.log("Number of sessions are: "+ num_of_sessions.length);
                // I observed that it is deleting oldest session, since new records are being pushed as if they are in Queue (FCFS Order)
                if(num_of_sessions.length >=5){
                    console.log("I am deleting one of the session........");
                    await Sessions.findOneAndDelete({Retailer_ID: {$eq: response.locals.strongParams['Retailer_ID'] }})
                }
                const deleted_session = await Sessions.findOne( {$and: [{Retailer_ID: {$eq: response.locals.strongParams['Retailer_ID']}}, {Session_ID: {$eq: sessionid}}] } );
                if(deleted_session){
                    await Sessions.updateOne({$and: [{Retailer_ID: response.locals.strongParams['Retailer_ID']}, {Session_ID: {$eq: null}}]}, {Session_ID: sessionid});
                }
                else {
                    await Sessions.create({Session_ID: sessionid, Retailer_ID: response.locals.strongParams['Retailer_ID']});
                }
                console.log("You now have a signed cookie");
                response.cookie("userCookie", sessionid, {signed: true, secure: false, httpOnly: true});
                response.send('You now have a cookie called userCookie!');
            }
            else{
                console.log("The credentials did not match");
                return response.sendStatus(400)
            }
        });
});

// --------------------------   Retrieving session ------------------------------------
// Check if the cookie sent is there in database
app.get('/retailer_home', [customMiddleware({requireCookie: true})], (request: Request, response: Response) =>{
    console.log("The user cookie is: "+ response.locals.userCookie);
    // Check if
    console.log("I am the homepage");
    return response.send("Good job! you are now in homepage")
});


// Route to log out of the session
// My log out is that I am completely wiping out session by making session id as null

// --------------------------   Deleting session ------------------------------------
app.get('/logout', [customMiddleware({requireCookie: true})], async (request: Request, response: Response) =>{
    const k = await Sessions.findOne({Session_ID: {$eq: response.locals.userCookie}});
     k.Session_ID = null;
    await k.save();
    response.clearCookie('userCookie');
    return response.send("You did a great logout").status(200)
});


app.listen(5000, ()=>{console.log("I am listening to the port 5000")});

// Pending work is to code 6 unit test cases to check functioning of each middlewares. Together 12 test cases.