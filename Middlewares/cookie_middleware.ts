import {NextFunction, Request, RequestHandler, Response} from 'express'

export function customMiddleware({ requireCookie = false}: { requireCookie: boolean }) : RequestHandler {
    console.log("I am a custom middleware");
    console.log("The require cookie value is: "+ requireCookie);
    return function middleware_function (request: Request, response: Response, next: NextFunction) {
        console.log("I am inside sub function");
        // Does the signed cookie exists, this is when logged in
        // To test this if condition, we definitely need to set a mock cookie
        if(request.signedCookies?.userCookie){
            console.log("I am in this request");
            console.log("The name of the cookie in middleware is : "+ request.signedCookies.userCookie)
            response.locals.userCookie = request.signedCookies.userCookie;
            return next()
        }
        // Cookie does not exists but require cookie is true
        else{
            if (requireCookie) {
                console.log("I am in a requireCookie true");
                console.log("Require cookie is: "+ requireCookie);
                response.send("Please do a login, your session expired").status(200);
                return next(new Error('Cookie was required for request but no cookie was found'));
            }
            console.log("I am in a requireCookie true 2");
            // If does not exist we ask to go to the path
            return next()
        }
    }
}


//  Middleware for authenticating
// The point of strong middleware is to ensure that all parameters sent through the route handler are valid
// We have included object to get rid of no sql attack
export function strongMiddleware(params: any): RequestHandler{
    return (request: Request, response: Response, next: NextFunction) => {
        let count = 0;
        console.log("I am going through strong middleware");
        // The following 3 lines are for testing purpose. They are not a part of middleware
        const {Retailer_ID, password} = request.body;
        console.log("Retailer_ID is: "+ Retailer_ID);
        console.log("Password is: "+ password);
        const strongParams: any = {};

        Object.entries(params).forEach(([weakParamKey, specifiedType])=> {
            //------------ Checking number type particularly  ------------------------
            if(typeof request.body[weakParamKey] === "number"){
                    console.log("I am checking a number");
                    console.log("The weak param key is: "+ request.body[weakParamKey]);
                    if( request.body[weakParamKey] > Number.MAX_VALUE
                        || request.body[weakParamKey] < Number.MIN_VALUE
                        || request.body[weakParamKey] === Infinity
                        || request.body[weakParamKey] === -Infinity) {
                        console.log("There is an exception");
                        return next(new Error("Problem with a number"))
                    }
                }
          // I just want to compare but not type check
            if ((request.body != null )  && (request.body[weakParamKey] != null)
                && request.body[weakParamKey] != undefined && typeof request.body[weakParamKey] !== "object"
                && request.body[weakParamKey] !== "") {
                if (typeof request.body[weakParamKey] === specifiedType) {
                    strongParams[weakParamKey] = request.body[weakParamKey];
                }
                else{
                    return next(new Error("Type Mismatch"));
                }
            }
            else return next(new Error("Exception occured"));
        });
        request.body = null;
        response.locals.strongParams = strongParams;
        return next();
    }
}

// Check for negative number in

export function abc(): RequestHandler{
    return (request: Request, response: Response, next: NextFunction)=>{
        console.log("I am a third middleware");
        return next();
    }
}


