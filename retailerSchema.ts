/*
Custom validation is declared by passing a validation function.
 */
import mongoose = require('mongoose');
import  {IRetailer} from './Retailer'
const Schema = mongoose.Schema;


const retailerSchema : mongoose.Schema<IRetailer> = new mongoose.Schema<IRetailer>({
    Company_name: {
        type: String,
        required: true,  // Built-In validation
    },
    Country_code:{
        type: Number,
        required: true  // Built-In validation
    },

    Phone_Number:{
        type: Number,
        validate:{
            validator: function (phone: number): boolean {
                // logic to check phone number
                // eg: 1234567890
                let count = 0;
                console.log("The phone number in model is: "+ phone);
                while(phone >0 ){
                    count +=1;
                    phone = Math.trunc(phone/10);
                    console.log(phone);
                    console.log("The count is: "+ count);
                }
                return (count === 10)
            },
            message: "Phone number should have 10 integers"
        },
        required: true
    },

    Address:{
        type: String,
        required: true
    },

    Country:{
        type: String,
        required: true
    },

    City: {
        type: String,
        validate: {
            validator: function (city: string): boolean {
                return city.length > 1
            }
        }
    },

    // Should have at least length of 4 characters
    Retailer_ID: {
        type: String,
        validate: {
            validator: function (id: string): boolean {
                return id.length >= 4
            },
            message: "retailer id should have a minimum length of 4"
        },
        required: true,
        unique: true,
    },



    // Password has lot of requirements. Should have an uppercase, lowercase, number and special characters.
    password: {
        type: String,
        validate: {
            validator: function (password: string): boolean {
                let re = /[a-z]/;
                let re1 = /[A-Z]/;
                let re2 = /[0-9]/;
                let re3 = /[!@#$%^&*]/;
                let t1 = re.test(password);
                let t2 = re1.test(password);
                let t3 = re2.test(password);
                let t4 = re3.test(password);
                return t1 && t2 && t3 && t4 && password.length > 8
            },
            message: "Password should have length of atleast 8, one uppercase, one lowercase and one number at least"
        },
        required: true

    }
});




// Getter and Setter
retailerSchema.virtual('full_Phone_number')
    .get(function (this: IRetailer) {
        return "+" + this.Country_code+ ""+ this.Phone_Number
    }).set(function (this: IRetailer, full_Phone_number: string) {
    const [countrycode, phonenumber]: string[] = full_Phone_number.split("");
    this.Country_code = parseInt(countrycode, 10)
});


export const Retailer = mongoose.model<IRetailer>('Retailer', retailerSchema);




