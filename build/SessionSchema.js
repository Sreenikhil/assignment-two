"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var mongoose = require("mongoose");
var Schema = mongoose.Schema;
var SessionSchema = new mongoose.Schema({
    Retailer_ID: {
        type: String,
    },
    Session_ID: {
        type: String
    }
});
exports.Sessions = mongoose.model('Sessions', SessionSchema);
