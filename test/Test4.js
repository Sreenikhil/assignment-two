const sinon = require('sinon');
const httpmock = require('node-mocks-http');
const assert = require('chai').assert;
const expect = require('chai').expect;
const customMiddleware = require('../Middlewares/cookie_middleware').customMiddleware;
const strongMiddleware = require('../Middlewares/cookie_middleware').strongMiddleware;
const nanoid = require('nanoid');

//--------------------------------------   custom Middleware test cases  ---------------------------------------------

//Checking if next function is called if true
describe("Middleware test 1", function(){
   it("Next function should be called when requireCookie is true", function(){
          const request = httpmock.createRequest({
           method : 'POST',
           url: '/session'
       });
       const response = httpmock.createResponse();
       const next = sinon.spy();
       customMiddleware({requireCookie: true})(request, response, next);
       sinon.assert.calledOnce(next)
   })
});

//Checking if next function is called if false
describe("Middleware test 2", function () {
    it("At any cost send cookies", function () {
        const request = httpmock.createRequest({
            method: 'POST',
            url: '/session'
        });
        const response = httpmock.createResponse();
        const next = sinon.spy();
        customMiddleware({requireCookie: false})(request, response, next);
        sinon.assert.calledOnce(next)
    });
});


//--------------------------------------------------------------------------------------

//Checking if next function is called with correct arguments when called with false
describe("Middleware test 3", function (){
    it("Next functions should have 0 arguments when called with false", function () {
        const request = httpmock.createRequest({
            method : 'POST',
            url: '/session'
        });
        const response = httpmock.createResponse();
        const next = sinon.spy();
        customMiddleware({requireCookie: false})(request, response, next);
        expect(next.getCall(0).args[0]).to.equal(undefined)
    })
});


//---------------------------------------------------------------------------

// If true and cookie does not exists then return error
describe("Middleware test 4", function(){
    it("When true and cookie is not set return error", function(){
        const request = httpmock.createRequest({
            method : 'POST',
            url: '/session',
        });
        const response = httpmock.createResponse();
        const next = sinon.spy();
        customMiddleware({requireCookie: true})(request, response, next);
        expect(next.firstCall.args[0].message).to.equal('Cookie was required for request but no cookie was found');
    })
});

// -----------------------------------------------------------------------------

// If cookie does not exist and require cookie is false then calls return
describe("Middleware test 5", function () {
    it("cookie does not exist and require cookie is false then just call return", function () {
        const request = httpmock.createRequest({
            method : 'POST',
            url: '/session'
        });
        const response = httpmock.createResponse();
        const next = sinon.spy();
        customMiddleware({requireCookie: false})(request, response, next);
        sinon.assert.called(next)
    })
});

// -------------- If cookie exists then set response.locals header, He/She is a valid user

describe("Middleware test 6", function () {
    it("Cookie exists", function () {
        const request = httpmock.createRequest({ signedCookies: { userCookie: "userCookies"} });
        const response = httpmock.createResponse();
        const next = sinon.spy();
        customMiddleware({requireCookie: false})(request, response, next);
        console.log("The usercookie is: "+  response.locals.userCookie);
        assert.equal(response.locals.userCookie, "userCookies")
    })
});


//--------------------------------------   Strong Middleware test cases  ---------------------------------------------

// To test strong parameter
// To check if it is making request.body null in the end

describe("Middleware test 6", function () {
    it("To check if request body is null", function () {
        const request = httpmock.createRequest({
            method : 'POST',
            url: '/session'
        });
        const response = httpmock.createResponse();
        const next = sinon.spy();
        strongMiddleware({Retail_ID: 'int'})(request, response, next)
        assert.isNull(request.body, "It is null")
        console.log("Request body is null")
    })
});

//-------------------------------------------------------
// Test case to check if it is doing proper type check

describe("Middleware test 7", function () {
    it("check if strong params in not empty", function () {
        const request = httpmock.createRequest({
            method : 'POST',
            url: '/session',
            body: {Retailer_ID: "Micro101", password: "Satya@1967"}
        });
        const response = httpmock.createResponse();
        const next = sinon.spy();
        strongMiddleware({Retailer_ID: 'string'})(request, response, next);
        assert.equal(response.locals.strongParams['Retailer_ID'], "Micro101");
        console.log("Response locals is working fine")
    })
});

//------------------------------------------------------------
// One more test case just to be sure

describe("Middleware test 8", function () {
    it("check if strong params in  empty", function () {
        const request = httpmock.createRequest({
            method : 'POST',
            url: '/session',
            body: {Retailer_ID: "Micro101", password: "Satya@1967"}
        });
        const response = httpmock.createResponse();
        const next = sinon.spy();
        strongMiddleware({Retailer_ID: 'number'})(request, response, next);
        assert.equal(response.locals.strongParams['Retailer_ID'], undefined);
        console.log("Response locals is working fine")
    })
});

//-----------------------------------------------------------------
// Check if next is called at least once
describe("Middleware test 9", function(){
    it("Next function should be called when requireCookie is true", function(){
        const request = httpmock.createRequest({
            method : 'POST',
            url: '/session',
            body: {Retailer_ID: "Micro101", password: "Satya@1967"}
        });
        const response = httpmock.createResponse();
        const next = sinon.spy();
        strongMiddleware({Retailer_ID: 'string'})(request, response, next);
        sinon.assert.calledOnce(next)
    })
});

//--------------------------------------------------------------------
//Checking if next function is called with correct arguments
describe("Middleware test 10", function (){
    it("Next functions should have 0 arguments when called with false", function () {
        const request = httpmock.createRequest({
            method : 'POST',
            url: '/session',
            body: {Retailer_ID: "Micro101", password: "Satya@1967"}
        });
        const response = httpmock.createResponse();
        const next = sinon.spy();
        strongMiddleware({Retailer_ID: 'string'})(request, response, next);
        expect(next.getCall(0).args[0]).to.equal(undefined)
    })
});

//--------------------------------------------------------------------
//Checking if request.body is null
describe("Middleware test 11", function () {
    it("Check if request body is null", function () {
        const request = httpmock.createRequest({
            method : 'POST',
            url: '/session',
            body: null
        });
        const response = httpmock.createResponse()
        const next = sinon.spy();
        strongMiddleware({})(request, response, next);
        console.log(response.locals.strongParams);
        assert.equal(Object.entries(response.locals.strongParams).length, 0)
    })
});

//------------------------------------------------------------------------
// We are not considering objects to guard against no sql attacks
describe("Middleware test 12", function () {
    it("check if request body parameter is object", function () {
        const request = httpmock.createRequest({
            method: 'POST',
            url: '/session',
            body: {Retailer_ID: {Retailer_ID: {$gt: 0}}, password: "Satya@1967"}
        })
            const response = httpmock.createResponse()
            const next = sinon.spy();
            strongMiddleware({Retailer_ID: 'object', password: 'string'})(request, response, next);
            console.log(response.locals.strongParams);
            assert.isUndefined(response.locals.strongParams['Retailer_ID'])
    })
});


