import mongoose = require('mongoose');
export interface ISessions extends mongoose.Document {
    Retailer_ID: string,
    Session_ID: string
}

