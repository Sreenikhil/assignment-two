"use strict";
exports.__esModule = true;
/*
Custom validation is declared by passing a validation function.
 */
var mongoose = require("mongoose");
var Schema = mongoose.Schema;
var retailerSchema = new mongoose.Schema({
    Company_name: {
        type: String,
        required: true
    },
    Country_code: {
        type: Number,
        required: true // Built-In validation
    },
    Phone_Number: {
        type: Number,
        validate: {
            validator: function (phone) {
                // logic to check phone number
                // eg: 1234567890
                var count = 0;
                console.log("The phone number in model is: " + phone);
                while (phone > 0) {
                    count += 1;
                    phone = Math.trunc(phone / 10);
                    console.log(phone);
                    console.log("The count is: " + count);
                }
                return (count === 10);
            },
            message: "Phone number should have 10 integers"
        },
        required: true
    },
    Address: {
        type: String,
        required: true
    },
    Country: {
        type: String,
        required: true
    },
    City: {
        type: String,
        validate: {
            validator: function (city) {
                return city.length > 1;
            }
        }
    },
    // Should have at least length of 4 characters
    Retailer_ID: {
        type: String,
        validate: {
            validator: function (id) {
                return id.length >= 4;
            },
            message: "retailer id should have a minimum length of 4"
        },
        required: true,
        unique: true
    },
    // Password has lot of requirements. Should have an uppercase, lowercase, number and special characters.
    password: {
        type: String,
        validate: {
            validator: function (password) {
                var re = /[a-z]/;
                var re1 = /[A-Z]/;
                var re2 = /[0-9]/;
                var re3 = /[!@#$%^&*]/;
                var t1 = re.test(password);
                var t2 = re1.test(password);
                var t3 = re2.test(password);
                var t4 = re3.test(password);
                return t1 && t2 && t3 && t4 && password.length > 8;
            },
            message: "Password should have length of atleast 8, one uppercase, one lowercase and one number at least"
        },
        required: true
    }
});
// Getter and Setter
retailerSchema.virtual('full_Phone_number')
    .get(function () {
    return "+" + this.Country_code + "" + this.Phone_Number;
}).set(function (full_Phone_number) {
    var _a = full_Phone_number.split(""), countrycode = _a[0], phonenumber = _a[1];
    this.Country_code = parseInt(countrycode, 10);
});
exports.Retailer = mongoose.model('Retailer', retailerSchema);
