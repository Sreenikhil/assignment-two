import * as mongoose from 'mongoose';
import  {IOrder} from './Order'


const orderSchema = new mongoose.Schema<IOrder>({

    //At most medicine ID has 7 digits
    Medicine_ID:{
        type: Number,
        validate:{
            validator: function (medid: number): boolean{
                return medid > 0 && medid < 9999999
            }
        },
        required: true
    },

    // Count cannot be less than 0

        count:{
        type: Number,
        validate:{
            validator: function (count: number): boolean {
                return count > 0
            }
        },
        required: true
    },

    // Notes length cannot be greater than 500 characters.
    Notes: {
        type: String,
        validate:{
            validator: function (notes: string): boolean {
                return notes.length < 500
            }
        }
    }
});

export const Order = mongoose.model<IOrder>('Order', orderSchema);