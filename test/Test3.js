const mongoose = require('mongoose');
const assert = require('chai').assert;
const Order = require('../orderSchema').Order;

// Testcase

const sample_order = new Order({Medicine_ID: 101, count: 100, Notes: "Please try to deliver it before next month" });

before(function (done) {
    const db_connect =  mongoose.connect("mongodb://localhost:27017/testdb", {useNewUrlParser: true} );
    const db = mongoose.connection;
    db.on('error', (error)=>{
        done(error)
    });
    db.once('open', function () {
        console.log("We are connected to the database");
        done()
    })
});
// -------------------  TO CHECK IF MEDICINE ID IS A NUMBER   -----------------

describe("To check if med id is number", function () {
    it("med id number check", function () {
        assert.isNotNaN(sample_order.Medicine_ID)
    })
});

// --------------   TO CHECK IF COUNT IS GREATER THAN 0    ------------------
// This one is causing mistake
describe("To check if count is greater than 0", function () {
    it("count is positive", function () {
        assert.isTrue(sample_order.count > 0)
    })
});

//  --------------  TO CHECK IF COUNT IS A NUMBER  -------------
describe("To check if count is number", function () {
    it("count number check", function () {
        assert.isNotNaN(sample_order.count)
    })
});

//  ------------------  TEST IF MEDICINE ID IS LESS THAN 7 DIGITS  -----------------
describe("Medicine ID is less than 7 characters", function () {
    it("less than 7 characters", function () {
        let medid = sample_order.Medicine_ID;
        assert.isTrue(medid > 0 && medid < 9999999 );
    })
});

// ----------  TEST IF MEDICINE ID EXISTS  --------------------
describe("Medicine id is present", function () {
    it("Required medicine id", function () {
        assert.exists(sample_order.Medicine_ID)
    })
});

// ---------------- TEST IF COUNT EXISTS ----------------------
// This is causing mistake
describe("count exists", function () {
    it("required count", function () {
        assert.exists(sample_order.count)
    })
});

// ----------------  DROP DATABASE  -------------------
after(async function(){
    try {
        await mongoose.connection.db.dropDatabase();
    }
    catch (e) {
        console.log(e)
    }
});